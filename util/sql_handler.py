from util.file_handler import FileHandler
import json
import pickle
import mysql.connector
from util.mysql_connection import MysqlConnection


def get_connection(new_user, new_password, new_host, new_database):
    return mysql.connector.connect(user= new_user, password=new_password,
                                        host=new_host,
                                        database=new_database)

class SqlHandler(FileHandler):
    def __init__(self, table, key, metapath):
        super().__init__()
        try:
            self.table = table
            self.key = key
            self.metapath = metapath

            with open(self.metapath, "rb") as metadata:
                db_metadata = json.load(metadata)

            self.cnx = get_connection(db_metadata["user"], db_metadata["password"], db_metadata["host"], db_metadata["database"])
            #self.cnx = MysqlConnection.getInstance().cnx
            self.cursor = self.cnx.cursor()
            self.data = self.load_data()
            self.column_names = self.cursor.column_names

        except mysql.connector.Error as err: 
            print("Failed: {}".format(err))
            self.cnx.close()

    def __exit__(self, type__, value, traceback):
        self.close()

    def __enter__(self):
        return self

    def close(self):
        if self.cnx:
            try:
                if self.__complete:
                    self.cnx.commit()
                else:
                    self.cnx.rollback()
            except Exception as e:
                raise Exception()
            finally:
                try:
                    self.cnx.close()
                except Exception as e:
                    raise Exception()

    def complete(self):
        self.__complete = True

    def load_data(self):
        return self.get_all()
    
    def get_one(self, id):
        try:
            self.cursor.execute("SELECT * FROM "+ self.table +" WHERE " + self.key + " = " + id)
            result = self.cursor.fetchall() 
            self.cnx.commit()
            return result
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
    
    def get_all(self):
        try:
            self.cursor.execute("SELECT * FROM  " + self.table)
            result = self.cursor.fetchall() 
            self.cnx.commit()
            return result
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
    
    def edit(self, obj, col, value):
        try:
            self.cursor.execute("UPDATE " + self.table + " SET " + col + " = '" + value + "' WHERE " + self.key + " = '" + str(obj[0]) + "'")
            self.cnx.commit()

        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.close()
            
    def delete_one(self, id):
        try:
            self.cursor.execute("DELETE FROM " + self.table + " WHERE " + self.key + " = '" + str(id) + "'")
            self.cnx.commit()
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.close()
    
    def insert(self, obj):
        try:
            inserted = self.cursor.execute("INSERT INTO " + self.table + " values " + " ( '" + "','".join(obj)  + "' )")
            self.cnx.commit()
            return inserted
        except mysql.connector.Error as err:
            print("Failed: {}".format(err))
            self.cnx.rollback()
            self.cnx.close()
            raise err
    
    def insert_many(self, obj):
        for i in obj:
            self.insert(i)
    
    def save(self, obj):
        pass