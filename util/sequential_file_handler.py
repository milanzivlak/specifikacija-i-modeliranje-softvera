from util.file_handler import FileHandler
import json
import pickle


class SequentialFileHandler():
    def __init__(self, file_path, meta_filepath):
        super().__init__()
        self.file_path = file_path
        self.meta_filepath = meta_filepath
        self.data = []
        self.metadata = {}
        self.load_data()

        #Napraviti binarnu pretragu posebno
    def load_data(self):
        with open((self.file_path), 'rb') as dfile:
            self.data = pickle.load(dfile)

        with open(self.meta_filepath) as meta:
            self.metadata = json.load(meta)

    def binary_search(self, id):
        bot = 0
        top = len(self.data)-1
        while bot <= top:
            mid = (top+bot)//2
            key = self.metadata["key"]
            if self.data[mid][key] == id:
                return mid
            elif self.data[mid][key] < id:
                bot = mid+1
            else:
                top = mid - 1
        return None

    def get_one(self,id):
        #TODO: sta ako binary search vrati None
        #Dobavljanje jednog elementa prema unaprijed utvrdjenom kljucu
        index = self.binary_search(id)
        
        return self.data[index]
            
            
    def get_all(self):
        #Dobavljanje svih elemenata 
        return self.data

    
    #Kad biramo datoteku, moze imati path to file knjiga_data i uz to navodi na type (serial, sequential)
    def insert(self, id, obj):
        """
        Ispitujemo da li vec postoji taj id, ukoliko postoji, ne izvrsi se insert.
        U suprotnom, trazimo poziciju gdje treba da bude umetnut.
        Ako je najveci, dodaje se na kraj, u suprotnom ide na indeks onoga koji je veci od njega
        """
        bot = 0
        top = len(self.data)-1
        biggest = True
        found = False
        while bot <= top:
            mid = (top+bot)//2
            
            key = self.metadata["key"]
            if self.data[mid][key] == id:
                print("Vec postoji element sa unesenim kljucem!")
                found = True
                break
            elif self.data[mid][key] > id:
                self.data.insert(mid, obj)
                biggest = False
                break
            else:
                bot = mid + 1
                
        if biggest == True and found == False:
            self.data.insert(mid+1, obj)
        
        if found == False:
            with open(self.file_path, 'wb') as f:
                pickle.dump(self.data, f)
        

    def insert_many(self, objects):
        """
        Prolazimo kroz kolekciju, uzimamo kljuc od svakog i pozivamo metodu insert.
        Insert za parametre uzima kljuc od tog objekta i sam objekat.
        """
        for obj in objects:
            key = obj[self.metadata["key"]]
            self.insert(key, obj)

        with open(self.file_path, 'wb') as f:
            pickle.dump(self.data, f)


    def edit(self, id, value):
        index = self.binary_search(id)
        self.data[index] = value

        with open(self.file_path, 'wb') as f:
            pickle.dump(self.data, f)

    def delete_one(self, id):
        #Brisanje elementa preko id-a
        index = self.binary_search(id)
        if index is not None:
            self.data.remove(self.data[index])
            with open(self.file_path, 'wb') as data:
                pickle.dump(self.data, data)
        else:
            print("Ne postoji element sa unesenim ID-em!")

    def print_all(self):
        lista = self.get_all()
        print(lista)
       # print(self.data)

    def save(self):
        with open(self.file_path, 'wb') as data:
            pickle.dump(self.data, data)
