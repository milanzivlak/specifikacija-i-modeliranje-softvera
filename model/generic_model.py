from PySide2 import QtCore, QtGui
import operator

class GenericModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None, metadata = [], elements = []):
        super().__init__(parent)
        self.elements = elements
        self.metadata = metadata
        self.read_only_column = 0
        

        for column in range(len(self.metadata["columns"])):
            if self.metadata["columns"][column] == self.metadata["key"]:
                self.read_only_column = column
    

    def get_element(self, index):
        #Vracanje elementa iz reda pod unesenim indeksom

        return self.elements[index.row()]

    

    def get_unique_data(self, index):
        subtable_key = self.metadata['subtable_key']
        selected = self.elements[index.row()]
       # print(selected[subtable_key])
        return selected[subtable_key]

    def get_subtable_metadata(self):
        metadata = self.metadata["subtable_key"]
        metadata += "_metadata.json"
        return metadata
    # def handler_type_check(self):
    #     with open(self.meta_filepath) as metadata:
    #         data = json.load(metadata)
    #         self.meta_data = data
    #         if data["type"] == "serijska":
    #             return "serijska"
    #         elif data["type"] == "sekvencijalna":
    #             return "sekvencijalna"


    def rowCount(self, index):
        return len(self.elements)
        
    def columnCount(self, index):
        return len(self.metadata['columns'])

    def data(self, index, role = QtCore.Qt.DisplayRole):
        #TODO: dodati obrade uloga(role)
        """
        Za svakog elementa u tabeli, prodje kroz kolone i popuni njegovim vrijednostima.
        """
        element = self.get_element(index)
        if role == QtCore.Qt.DisplayRole:
            ret_column = self.metadata['columns'][index.column()]
            return element[ret_column]
            #return getattr(element, self.metadata['columns'][index.column()])
        if role == QtCore.Qt.BackgroundRole and index.column() == self.read_only_column:
            return QtGui.QBrush(QtGui.QColor(201,175,175))
        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.metadata['columns'][section]

        return None

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        """
        Unesenu vrijednost postavlja kao novu za izabrani element, onaj koji se nalazi na datom indeksu.
        """
        element = self.get_element(index)
        if value == " ":
            return False

        if role == QtCore.Qt.EditRole:
            element[self.metadata['columns'][index.column()]] = value
            return True

        return False

    def flags(self, index):
        if index.column() == self.read_only_column:
            return super().flags(index) | QtCore.Qt.ItemIsSelectable
        else:
            return super().flags(index) | QtCore.Qt.ItemIsEditable


    def sort(self, col, order):
        """
        Sortiranje glavne tabele preko izabrane kolone.
        """
        sort_by = self.metadata['columns'][col]
        self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
        self.elements = sorted(self.elements, key=operator.itemgetter(sort_by))        
        if order == QtCore.Qt.DescendingOrder:
            self.elements.reverse()
        self.emit(QtCore.SIGNAL("layoutChanged()"))